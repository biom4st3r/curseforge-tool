import requests
import json
import enum
import sys
from bs4 import BeautifulSoup as bs
from typing import Any, Iterable, List, Set, Dict, Tuple, Optional, Union

args = sys.argv

if len(args) < 2:
    print('Proper sythax: python curseforge_tool.py [test_aval,getchangelog]')
    exit()


class fileStatus(enum.Enum):
    UNKNOWN0 = 0,
    UNKNOWN1 = 1,
    UNKNOWN2 = 2,
    UNKNOWN3 = 3,
    Approved = 4,
    REJECTED = 5,
    UNKNOWN6 = 6,
    DELETED = 7,
    ARCHIVED = 8,


headers = {
    'User-Agent': 'Mozilla/5.0 CK={} (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko'
}
manifest = json.loads(open('manifest.json', 'r+').read())
files = manifest['files']


def getFileData(projectid: int, fileid: int):
    return f'https://addons-ecs.forgesvc.net/api/v2/addon/{projectid}/file/{fileid}/'


def getFileChangeLog(projectid: int, fileid: int):
    return f'https://addons-ecs.forgesvc.net/api/v2/addon/{projectid}/file/{fileid}/changelog'


def testAval():
    for x in files:
        r: requests.Response = requests.get(getFileData(x["projectID"], x["fileID"]), headers=headers)
        if r.status_code != 200:
            print(x)
            print(r.status_code)
            print('bad')
            print(f'https://addons-ecs.forgesvc.net/api/v2/addon/{x["projectID"]}')
            break
        else:
            data: Dict[str, Any] = json.loads(r.content)
            if data['fileStatus'] != 4 and data['isAvailable']:
                print('bad')
                print(x)
                print(r.status_code)
                print(f'https://addons-ecs.forgesvc.net/api/v2/addon/{x["projectID"]}')
                break
            print('good')


def getChangeLog():
    t: str = ''

    for x in files:

        r: requests.Response = requests.get(getFileData(x["projectID"], x["fileID"]), headers=headers)
        data: Dict[str, Any] = json.loads(r.content)
        print(data['displayName'])
        t += '* ' + data['displayName']
        t += '\n'
        r = requests.get(getFileChangeLog(x["projectID"], x["fileID"]), headers=headers)
        soup: bs = bs(r.text, features="html.parser")
        for d in soup.find_all():
            if len(d.text) > 1:
                t += d.text
        t += '\n\n\n'
    with open('big-changelog.log', 'wb+') as file:
        file.write(t.encode('utf-8'))


commands = {
    'test_aval': testAval,
    'getchangelog': getChangeLog
}

commands[args[1]]()
